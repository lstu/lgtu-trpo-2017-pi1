'use strict';

var url = require('url');


var Modifications_type = require('./Modifications_typeService');


module.exports.getModif = function getModif (req, res, next) {
  Modifications_type.getModif(req.swagger.params, res, next);
};

module.exports.getModificationsById = function getModificationsById (req, res, next) {
  Modifications_type.getModificationsById(req.swagger.params, res, next);
};

module.exports.getVariantList = function getVariantList (req, res, next) {
  Modifications_type.getVariantList(req.swagger.params, res, next);
};

module.exports.postModType = function postModType (req, res, next) {
  Modifications_type.postModType(req.swagger.params, res, next);
};

module.exports.postOptions = function postOptions (req, res, next) {
  Modifications_type.postOptions(req.swagger.params, res, next);
};
