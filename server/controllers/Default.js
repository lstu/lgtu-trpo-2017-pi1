'use strict';

var url = require('url');


var Default = require('./DefaultService');


module.exports.addselect = function addselect (req, res, next) {
  Default.addselect(req.swagger.params, res, next);
};

module.exports.clientGET = function clientGET (req, res, next) {
  Default.clientGET(req.swagger.params, res, next);
};

module.exports.findClientById = function findClientById (req, res, next) {
  Default.findClientById(req.swagger.params, res, next);
};

module.exports.manufacturerGET = function manufacturerGET (req, res, next) {
  Default.manufacturerGET(req.swagger.params, res, next);
};

module.exports.modelId_manGET = function modelId_manGET (req, res, next) {
  Default.modelId_manGET(req.swagger.params, res, next);
};

module.exports.possible_optionsId_modelGET = function possible_optionsId_modelGET (req, res, next) {
  Default.possible_optionsId_modelGET(req.swagger.params, res, next);
};
