'use strict';

exports.getModif = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getModificationsById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * modification_id (Integer)
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getVariantList = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.postModType = function(args, res, next) {
  /**
   * parameters expected in the args:
  * modification_id (Modifications_type)
  **/
  // no response value expected for this operation
  res.end();
}

exports.postOptions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * svariant_id (Variant_list)
  **/
  // no response value expected for this operation
  res.end();
}

