'use strict';

exports.getEngine = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.postEngine = function(args, res, next) {
  /**
   * parameters expected in the args:
  * engine_id (Engine)
  **/
  // no response value expected for this operation
  res.end();
}

