'use strict';

var url = require('url');


var Engine = require('./EngineService');


module.exports.getEngine = function getEngine (req, res, next) {
  Engine.getEngine(req.swagger.params, res, next);
};

module.exports.postEngine = function postEngine (req, res, next) {
  Engine.postEngine(req.swagger.params, res, next);
};
