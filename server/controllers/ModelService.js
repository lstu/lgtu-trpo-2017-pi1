'use strict';

exports.getModel = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getModelById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * mod_id (Integer)
  **/
    var examples = {};
    if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.postModel = function(args, res, next) {
  /**
   * parameters expected in the args:
  * mod_id (Model)
  **/
  // no response value expected for this operation
  res.end();
}

