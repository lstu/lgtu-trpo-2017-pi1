'use strict';

var url = require('url');


var Model = require('./ModelService');


module.exports.getModel = function getModel (req, res, next) {
  Model.getModel(req.swagger.params, res, next);
};

module.exports.getModelById = function getModelById (req, res, next) {
  Model.getModelById(req.swagger.params, res, next);
};

module.exports.postModel = function postModel (req, res, next) {
  Model.postModel(req.swagger.params, res, next);
};
